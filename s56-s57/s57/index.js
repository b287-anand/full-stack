// Note: You can use the snippets here to demonstrate it in the browser console.

// Question: What would be the output of the loop?
// The output will be as follows
// 1
// 2
// 3
// 4


for (let i = 1; i < 5; i++) {
  console.log(i * 1);
}

// Question: What would be the problem in the code snippet?
// The output will be like    ['John', 'Paul', 'George', 'Ringo', undefined]
// One undefined will also be added in it.

let students = ['John', 'Paul', 'George', 'Ringo'];

console.log('Here are the graduating students:');

for (let count = 0; count <= students.length; i++) {
  console.log(students[count]);
}

// Question: What would be the console output of the function?
// I was given 3 french hens.

function checkGift(day) {
  let gifts = [
    'partridge in a pear tree',
    'turtle doves',
    'french hens',
    'golden rings'
  ];

  if (day > 0 && day < 4) {
    return `I was given ${day} ${gifts[day-1]}`;
  } else {
    return `No gifts were given`;
  }
}

checkGift(3);

// What would be the problem in the code snippet?
// Due to the duplicate id of the objects this code will behave unexpectedly and may print the two objects with same id.


let items = [
  {
    id: 1,
    name: 'Banana',
    description: 'A yellow fruit',
    price: 15.00,
    category: 2
  },
  {
    id: 2,
    name: 'Pork Cutlet',
    description: 'Japanese kurobuta',
    price: 15.00,
    category: 1
  }, 
  {
    id: 1,
    name: 'Sweet Potato',
    description: 'Best when roasted',
    price: 15.00,
    category: 3
  }
];

for (let i = 0; i < items.length; i++) {
  console.log(`
    Name: ${items[i].name}
    Description: ${items[i].description}
    Price: ${items[i].price}
  `);
}

// Question: What would be the output?
//Current row:1,Current col:1
//Current row:2,Current col:1,Current row:2,Current col:2


for (let row = 1; row < 3; row++) {
  for (let col = 1; col <= row; col++) {
    console.log(`Current row: ${row}, Current col: ${col}`);
  }
}

// Question: What would be the problem in the code snippet?
// It will have syntax errors because assignment operator is used instead of equality opertaor.

function checkLeapYear(year) {
  if (year % 4 = 0) {
    if (year % 100 = 0) {
      if (year % 400 = 0) {
        console.log('Leap year');
      } else {
        console.log('Not a leap year');
      }
    } else {
      console.log('Leap year');
    }
  } else {
    console.log('Not a leap year');
  }
}

checkLeapYear(1999);

// Question: Given the array below, how can the last student's English grade be displayed?
//records[2].subjects[0].grade

let records = [
  {
    id: 1,
    name: 'Brandon',
    subjects: [
      { name: 'English', grade: 98 },
      { name: 'Math', grade: 66 },
      { name: 'Science', grade: 87 }
    ]
  },
  {
    id: 2,
    name: 'Jobert',
    subjects: [
      { name: 'English', grade: 87 },
      { name: 'Math', grade: 99 },
      { name: 'Science', grade: 74 }
    ]
  },
  {
    id: 3,
    name: 'Junson',
    subjects: [
      { name: 'English', grade: 60 },
      { name: 'Math', grade: 99 },
      { name: 'Science', grade: 87 }
    ]
  }
];

// Question: What would be the problem in the code snippet?
//Dividing by zero is not mathematically defined so it will give unexpected errors

function checkDivisibility(dividend, divisor) {
  if (dividend % divisor == 0) {
    console.log(`${dividend} is divisible by ${divisor}`);
  } else {
    console.log(`${dividend} is not divisible by ${divisor}`);
  }
}

checkDivisibility(100, 0);
