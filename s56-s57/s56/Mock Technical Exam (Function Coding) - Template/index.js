function countLetter(letter, sentence) {
    let result = 0;
    if(letter.length===1){
        let i;
        for(i=0;i<sentence.length;i++){
            if(letter===sentence[i]){
                result++;
            }
        }
        return result;
    }
    else{
        return undefined;
    }
    

    // Check first whether the letter is a single character.
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    // If letter is invalid, return undefined.

    
}


function isIsogram(text) {
    let t=text.toLowerCase()
    for(i=0;i<t.length;i++){
        for(j=i+1;j<t.length;j++){
            if(t[i]===t[j]){
                return false;
            }
           
        }

        
    }
    return true;
    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.

    
}

function purchase(age, price) {
    if(age<13){
        return undefined;
    }
    else if(age<=21 ){
       let d= (price*0.8);
        return d.toFixed(2);
    }
    else if(age<=64){
       let c=(price);
        return c.toFixed(2);
    }else{
        let  a=(price*0.8);
        return a.toFixed(2);
    }
    // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.
    
}

function findHotCategories(items) {
    const d=[];

    for(i=0;i<items.length;i++){
        if(items[i].stocks===0){
             if (!d.includes(items[i].category)) {
                d.push(items[i].category);
            }
        }
    }
    return d;
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.

}

function findFlyingVoters(candidateA, candidateB) {
    const f=[];
    for(i=0;i<candidateA.length;i++){
        for(j=0;j<candidateB.length;j++){
            if(candidateA[i]===candidateB[j]){
                f.push(candidateA[i]);
                break;
            }

        }
    }
    return f;
    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
    
}

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};