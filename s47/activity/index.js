// The "document" refers to the whole webpage.
// The "querySelector" is used to select a specific object (HTML element) from the document. (webpage)

const txtFirstName = document.querySelector('#txt-first-name');
const spanFullName = document.querySelector('#span-full-name');
const txtLastName=  document.querySelector('#txt-last-name');
// Alternatively, we can use the getElement functions to retrieve the element
	// document.getElementById
	// document.getElementByClassName
	// document.getElementByTagName



txtFirstName.addEventListener('keyup', (event) => {
  const firstName = txtFirstName.value;
  const lastName = txtLastName.value;
  const fullName = `${firstName} ${lastName}`;
  spanFullName.innerHTML = fullName;
});


txtLastName.addEventListener('keyup', (event) => {
  const firstName = txtFirstName.value;
  const lastName = txtLastName.value;
  const fullName = `${firstName} ${lastName}`;
  spanFullName.innerHTML = fullName;
});
